# README #

### Opis funkcjonalny ###
Po rozpoczęciu rozgrywki użytkownik ma możliwość ustawienia swoich statków na planszy. Wyświetla się lista pozostałych statków do rozmieszczenia oraz wskazówka jak należy to robić. Należy podać oddzielony spacjami ciąg znaków definiujący rozmieszczenie statku.
Po rozmieszczeniu statków przez gracza wczytywane jest rozmieszczenie dla AI i rozpoczyna się gra. Wyświetlane są dwie plansze: plansza z ustawieniem statków gracza, do której strzelać będzie AI, oraz pusta plansza, do której będzie strzelał gracz.
Strzał odbywa się przez podanie koordynatów pola. Jeśli gracz trafi dostaje następny ruch. Jeśli spudłuje kolej przechodzi na AI.
Jeśli gracz lub AI trafi wszystkie statki przeciwnika wygrywa i gra się kończy.


### Informacje techniczne ###
Aplikacja konsolowa do gry w statki napisana w oparciu o język JAVA przy użyciu IntelliJ IDEA. Podczas pracy wykorzystano rozproszony system kontroli wersji Git na platformie Bitbucket.
Podczas prac nad projektem napotkaliśmy błąd, przez który przy wygranej gracza gra nie kończyła się. Zaznaczyliśmy potencjalnie sprawiający kłopot element kodu i rozpoczeliśmy proces debugowania. W jego trakcie okazało się, że błąd jest w niepoprawnie umiejscowionym wywołaniu metody wykonującej ponowny ruch gracza po trafieniu w statek przeciwnika.


### Podział prac ###

| Mateusz Tarchała | Jakub Goch |
|------------------|:----------:|
| Metoda generująca planszę AI | Klasa Plansza i metoda wyświetlająca planszę |
| Metody służące do generacji planszy gracza | Klasa Rozgrywka i wyświetlanie plansz |
| Funkcjonalność odpowiedzialna za strzał gracza | Metody służące do oddania strzału przez AI |


